// This program was developped by Daniel Audet and uses sections of code  
// from http://math.hws.edu/eck/cs424/notes2013/19_GLSL.html
//
//  It has been adapted to be compatible with the "MV.js" library developped
//  for the book "Interactive Computer Graphics" by Edward Angel and Dave Shreiner.
//

"use strict";

var gl;   // The webgl context.

var CoordsLoc;       // Location of the coords attribute variable in the standard texture mappping shader program.
var NormalLoc;
var TexCoordLoc;

var ka, ks, kd;

var ProjectionLoc;     // Location of the uniform variables in the standard texture mappping shader program.
var ModelviewLoc;
var NormalMatrixLoc;


var projection;   //--- projection matrix
var modelview;    // modelview matrix
var flattenedmodelview;    //--- flattened modelview matrix

var normalMatrix = mat3();  //--- create a 3X3 matrix that will affect normals

var rotator;   // A SimpleRotator object to enable rotation by mouse dragging.

var sphere, cylinder, box, teapot, disk, torus, cone, radar, cutCone, moitieFourche, octogone, supportRadar, semiSphere;  // model identifiers

var prog;  // shader program identifier

var lightPosition = vec4(20.0, 20.0, 100.0, 1.0);

var lightAmbient = vec4(1.0, 1.0, 1.0, 1.0);
var lightDiffuse = vec4(1.0, 1.0, 1.0, 1.0);
var lightSpecular = vec4(1.0, 1.0, 1.0, 1.0);

var materialAmbient = vec4(0.74, 0.56, 0.56, 1.0); //rgb textures
var materialDiffuse = vec4(0.48, 0.55, 0.69, 1.0);
var materialSpecular = vec4(0.48, 0.55, 0.69, 1.0);
var materialShininess = 100.0;

var ambientProduct, diffuseProduct, specularProduct;

function handleLoadedTexture(texture) {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

    texture.isloaded = true;

    render();  // Call render function when the image has been loaded (to insure the model is displayed)

    gl.bindTexture(gl.TEXTURE_2D, null);
}
var flameTexture;
var gridTexture;
var metalTexture;
var radarTexture;
var canonTexture;
var cockpitTexture;
var skybox;
var sunTexture;
var earthTexture;
var marsTexture;
var moonTexture;
var moiTexture;

function initTexture() {
	//define skybox texture
	skybox = gl.createTexture();
    skybox.isloaded = false;  // this class member is created only to check if the image has been loaded

    skybox.image = new Image();
    skybox.image.onload = function () {
        handleLoadedTexture(skybox)
    }

    skybox.image.src = "skybox.jpg";
	
	//define sun texture
	sunTexture = gl.createTexture();
    sunTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    sunTexture.image = new Image();
    sunTexture.image.onload = function () {
        handleLoadedTexture(sunTexture)
    }
	sunTexture.image.src = "sun.jpg";
	
	//define earth texture
	earthTexture = gl.createTexture();
    earthTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    earthTexture.image = new Image();
    earthTexture.image.onload = function () {
        handleLoadedTexture(earthTexture)
    }
	earthTexture.image.src = "earth.jpg";
	
	//define moon texture
	moonTexture = gl.createTexture();
    moonTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    moonTexture.image = new Image();
    moonTexture.image.onload = function () {
        handleLoadedTexture(moonTexture)
    }
	moonTexture.image.src = "moon.jpg";
	
	//define mars texture
	marsTexture = gl.createTexture();
    marsTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    marsTexture.image = new Image();
    marsTexture.image.onload = function () {
        handleLoadedTexture(marsTexture)
    }
	marsTexture.image.src = "mars.jpg";
	
	//define texture cube signé
	moiTexture = gl.createTexture();
    moiTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    moiTexture.image = new Image();
    moiTexture.image.onload = function () {
        handleLoadedTexture(moiTexture)
    }
	moiTexture.image.src = "signature.jpg";
	
    // define first texture
    flameTexture = gl.createTexture();
    flameTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    flameTexture.image = new Image();
    flameTexture.image.onload = function () {
        handleLoadedTexture(flameTexture)
    }

    flameTexture.image.src = "blue.jpg";

    // define second texture
    gridTexture = gl.createTexture();
    gridTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    gridTexture.image = new Image();
    gridTexture.image.onload = function () {
        handleLoadedTexture(gridTexture)
    }

    gridTexture.image.src = "canon.jpg";

    // define third texture
    metalTexture = gl.createTexture();
    metalTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    metalTexture.image = new Image();
    metalTexture.image.onload = function () {
        handleLoadedTexture(metalTexture)
    }

    metalTexture.image.src = "wall2.jpg";

	// define fourth texture
    radarTexture = gl.createTexture();
    radarTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    radarTexture.image = new Image();
    radarTexture.image.onload = function () {
        handleLoadedTexture(radarTexture)
    }
	radarTexture.image.src = "radar.jpg";
	
	// define fifth texture
    canonTexture = gl.createTexture();
    canonTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    canonTexture.image = new Image();
    canonTexture.image.onload = function () {
        handleLoadedTexture(canonTexture)
    }
	canonTexture.image.src = "metal050.jpg";
	
	// define sixth texture
    cockpitTexture = gl.createTexture();
    cockpitTexture.isloaded = false;  // this class member is created only to check if the image has been loaded

    cockpitTexture.image = new Image();
    cockpitTexture.image.onload = function () {
        handleLoadedTexture(cockpitTexture)
    }

    cockpitTexture.image.src = "cockpit.jpg";
	
}

function render() {
    gl.clearColor(0.79, 0.76, 0.27, 1);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    projection = perspective(70.0, 1.0, 1.0, 200.0);

    //--- Get the rotation matrix obtained by the displacement of the mouse
    //---  (note: the matrix obtained is already "flattened" by the function getViewMatrix)
    flattenedmodelview = rotator.getViewMatrix();
    modelview = unflatten(flattenedmodelview);

	normalMatrix = extractNormalMatrix(modelview);
		
    var initialmodelview = modelview;

    //  Select shader program 
    gl.useProgram(prog);

    gl.uniform4fv(gl.getUniformLocation(prog, "ambientProduct"), flatten(ambientProduct));
    gl.uniform4fv(gl.getUniformLocation(prog, "diffuseProduct"), flatten(diffuseProduct));
    gl.uniform4fv(gl.getUniformLocation(prog, "specularProduct"), flatten(specularProduct));
    gl.uniform1f(gl.getUniformLocation(prog, "shininess"), materialShininess);

    gl.uniform4fv(gl.getUniformLocation(prog, "lightPosition"), flatten(lightPosition));

    gl.uniformMatrix4fv(ProjectionLoc, false, flatten(projection));  // send projection matrix to the new shader program

	gl.enableVertexAttribArray(CoordsLoc);
    gl.enableVertexAttribArray(NormalLoc);
    gl.enableVertexAttribArray(TexCoordLoc);

	//<!-------------------------------------------------------------Vaisseau---------------------------------------------------------------------->
	
    //  boîte sphérique principale du vaisseau
    modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 0.0, 0.0));
    modelview = mult(modelview, rotate(0.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(1.5, 0.25, 1.5));
	//application de la texture
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, metalTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 0);
	}
    sphere.render();


    //  boîte cylindrique principale du vaisseau
	 modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 0.0, 0.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(1.5, 1.5, 0.1));
    cylinder.render();
	
	//  cylindre central
	 modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 0.0, 0.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.5, 0.5, 0.25));
    cylinder.render();
	
	//  cylindres latéraux
	 modelview = initialmodelview;
    modelview = mult(modelview, translate(-13.5, 0.0, 0));
    modelview = mult(modelview, rotate(90.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(13.5, 0.0, 0));
    modelview = mult(modelview, rotate(90.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	// octogones latéraux, enveloppant les cylindres
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-10.0, 0.0, 0));
    modelview = mult(modelview, rotate(90.0, 0, 1, 0));
	modelview = mult(modelview, rotate(22.0, 0, 0, 1));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.25, 0.25, 0.45));
    octogone.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(10.0, 0.0, 0));
    modelview = mult(modelview, rotate(90.0, 0, 1, 0));
	modelview = mult(modelview, rotate(22.0, 0, 0, 1));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.25, 0.25, 0.45));
    octogone.render();
	
	
	
	//<-----------------------------------------------------------FOURCHE AVANT----------------------------------------------------------------------------->
	
	//haut de la fourche
	modelview = initialmodelview;
    modelview = mult(modelview, translate(0, 2.1, 11.2));
    modelview = mult(modelview, rotate(0.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.25, 0.07, 1.2));
    box.render();
	
	//bas de la fourche
	modelview = initialmodelview;
    modelview = mult(modelview, translate(0, -2.1, 11.2));
    modelview = mult(modelview, rotate(0.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.25, 0.07, 1.2));
    box.render();
	
	//droite de la fourche
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-6.0, 0.0, 15.0));
    modelview = mult(modelview, rotate(0.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.8, 0.18, 1.2));
    moitieFourche.render();
	
	//droite de la fourche
	modelview = initialmodelview;
    modelview = mult(modelview, translate(6.0, 0.0, 15.0));
    modelview = mult(modelview, rotate(180, 0, 0, 1));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.8, 0.18, 1.2));
    moitieFourche.render();
	
	//<-----------------------------------------------------------CANON CENTRAL----------------------------------------------------------------------------->
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 2.1, 0.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.05));
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, gridTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 2);
	}
    cylinder.render();
	
	//  boîte du canon central
	 modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 3.0, 0.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.20, 0.15, 0.10));
	//application de la texture
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE8);
    gl.bindTexture(gl.TEXTURE_2D, canonTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 8);
	}
    box.render();
	
	//canons
	modelview = initialmodelview;
    modelview = mult(modelview, translate(0.35, 3.0, 1.5));
    modelview = mult(modelview, rotate(0.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
	modelview = mult(modelview, scale(0.03, 0.03, 0.1));
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE9);
    gl.bindTexture(gl.TEXTURE_2D, canonTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 9);
	}
    cylinder.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-0.35, 3.0, 1.5));
    modelview = mult(modelview, rotate(0.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
	modelview = mult(modelview, scale(0.03, 0.03, 0.1));
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE10);
    gl.bindTexture(gl.TEXTURE_2D, canonTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 10);
	}
    cylinder.render();
	
	//<!-------------------------------------------------------------RADAR---------------------------------------------------------------------->
	//support radar
	modelview = initialmodelview;
    modelview = mult(modelview, translate(5.5, 2.5, 7.0));
    modelview = mult(modelview, rotate(-90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.2, 0.2, 0.2));
	//application de la texture
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE6);
    gl.bindTexture(gl.TEXTURE_2D, gridTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 6);
	}
    supportRadar.render();
	
	//plaque noire où est posé le radar
	modelview = initialmodelview;
    modelview = mult(modelview, translate(5.5, 1.2, 7.0));
    modelview = mult(modelview, rotate(-90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.12, 0.12, 0.1));
    cylinder.render();
	
	//support radar vide à gauche du vaisseau
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-5.5, 1.2, 7.0));
    modelview = mult(modelview, rotate(-90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.12, 0.12, 0.1));
    cylinder.render();
	
	//radar
	modelview = initialmodelview;
    modelview = mult(modelview, translate(6.8, 4.3, 8.1));
    modelview = mult(modelview, rotate(-38.0, 1, 0, 0));
	modelview = mult(modelview, rotate(45.0, 0, 1, 0));
	modelview = mult(modelview, rotate(0.0, 0, 0, 1));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.16, 0.16, 0.16));
	//application de la texture
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE7);
    gl.bindTexture(gl.TEXTURE_2D, radarTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 7);
	}
    semiSphere.render();
	
	//cone au centre du radar
	modelview = initialmodelview;
    modelview = mult(modelview, translate(6.2, 3.8, 7.5));
    modelview = mult(modelview, rotate(-38.0, 1, 0, 0));
	modelview = mult(modelview, rotate(45.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.05, 0.05, 0.02));
	//application de la texture
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, metalTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 0);
	}
    cone.render();
	//<-----------------------------------------------------------Détails----------------------------------------------------------------------------->
	//rectangles fourche
	//  gauche
	 modelview = initialmodelview;
    modelview = mult(modelview, translate(-5.6, 0.5, 13.5));
    modelview = mult(modelview, rotate(90.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.20, 0.15, 0.10));
	box.render();
	
	//  droite
	 modelview = initialmodelview;
    modelview = mult(modelview, translate(5.6, 0.5, 13.5));
    modelview = mult(modelview, rotate(90.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.20, 0.15, 0.10));
	box.render();
	
	//<-----------------------------------------------------------COCKPIT----------------------------------------------------------------------------->
	//bras avant droit tenant le cockpit
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-11, 0.8, 8.8));
    modelview = mult(modelview, rotate(0.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	//cylindre reliant le cockpit
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-7.0, 0.8, 4));
    modelview = mult(modelview, rotate(-45.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.6));
    cylinder.render();
	
	//cockpit
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-11, 0.8, 11.8));
    modelview = mult(modelview, rotate(270.0, 0, 0, 1));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
	//modelview=mult(modelview, materialAmbient);
	//application de la texture
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
	materialAmbient = vec4(0.2, 0.2, 0.2, 1.0);
	materialDiffuse = vec4(0.2, 0.2, 0.2, 1.0);
	materialSpecular = vec4(0.2, 0.2, 0.2, 1.0);
    gl.activeTexture(gl.TEXTURE12);
    gl.bindTexture(gl.TEXTURE_2D, cockpitTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 12);
	}
    cutCone.render();
	
	//<-----------------------------------------------------------DETAILS----------------------------------------------------------------------------->
	/**cercles à l'arrière du vaisseau*/
	//première rangée
	modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 0.9, -7.2));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, gridTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 2);
	}
    cylinder.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(3.5, 0.9, -6.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-3.5, 0.9, -6.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	//deuxième rangée
	modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 0.6, -10.4));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(4.5, 0.6, -9.2));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-4.5, 0.6, -9.2));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.15, 0.15, 0.15));
    cylinder.render();
	
	
	
	//ajouts partie 2
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(5.9, 0.5, 17.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.07, 0.07, 0.05));
    cylinder.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-5.9, 0.5, 17.0));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.07, 0.07, 0.05));
    cylinder.render();
	
	/**rectangles arrières*/
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-9.0, 0.85, -8.0));
    modelview = mult(modelview, rotate(-45.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.2, 0.15, 0.15));
    box.render();
	
	modelview = initialmodelview;
    modelview = mult(modelview, translate(9.0, 0.85, -8.0));
    modelview = mult(modelview, rotate(45.0, 0, 1, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.2, 0.15, 0.15));
    box.render();
	
	/**réacteur*/
	//  boîte cylindrique principale du vaisseau
	 modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 0.0, -1.5));
    modelview = mult(modelview, rotate(90.0, 1, 0, 0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(1.4, 1.4, 0.08));
	if (metalTexture.isloaded && flameTexture.isloaded && gridTexture.isloaded && radarTexture.isloaded && canonTexture.isloaded && cockpitTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, flameTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 1);
	}
    cylinder.render();
	
	/**----------------------------------------------------------SKYBOX --------------------------------------------------------*/
    modelview = initialmodelview;
    modelview = mult(modelview, translate(0.0, 0.0, 0.0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(15.0, 15.0, 15.0));
	//application de la texture
	if (skybox.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE42);
    gl.bindTexture(gl.TEXTURE_2D, skybox);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 42);
	}
    box.render();
	
	
	/**----------------------------------------------------------PLANETS --------------------------------------------------------*/
	//sun
	modelview = initialmodelview;
    modelview = mult(modelview, translate(36.0, 48.0, 55.0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(3.0, 3.0, 3.0));
	//application de la texture
	if (sunTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE43);
    gl.bindTexture(gl.TEXTURE_2D, sunTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 43);
	}
    sphere.render();
	
	
	//earth
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-16.0, 28.0, 35.0));
	modelview= mult(modelview, rotate(0, 0, 1, 60.0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(1.0, 1.0, 1.0));
	//application de la texture
	if (earthTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE44);
    gl.bindTexture(gl.TEXTURE_2D, earthTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 44);
	}
    sphere.render();
	
	
	//moon
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-10.0, 18.0, 25.0));
	modelview= mult(modelview, rotate(0, 0, 1, 60.0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.2, 0.2, 0.2));
	//application de la texture
	if (moonTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE45);
    gl.bindTexture(gl.TEXTURE_2D, moonTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 45);
	}
    sphere.render();
	
	
	//mars
	modelview = initialmodelview;
    modelview = mult(modelview, translate(-60.0, -51.0, -25.0));
	modelview= mult(modelview, rotate(0, 0, 1, 60.0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.9, 0.9, 0.9));
	//application de la texture
	if (marsTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE46);
    gl.bindTexture(gl.TEXTURE_2D, marsTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 46);
	}
    sphere.render();
	
	//cube signé
	modelview = initialmodelview;
    modelview = mult(modelview, translate(20.0, 10.0, 5.0));
	modelview= mult(modelview, rotate(0, 0, 1, 60.0));
    normalMatrix = extractNormalMatrix(modelview);  // always extract the normal matrix before scaling
    modelview = mult(modelview, scale(0.9, 0.9, 0.9));
	//application de la texture
	if (marsTexture.isloaded)  // if texture identifiers have been assigned
	{
    gl.activeTexture(gl.TEXTURE47);
    gl.bindTexture(gl.TEXTURE_2D, moiTexture);
    // Send texture to sampler
    gl.uniform1i(gl.getUniformLocation(prog, "texture"), 47);
	}
    box.render();
	
	}


function unflatten(matrix) {
    var result = mat4();
    result[0][0] = matrix[0]; result[1][0] = matrix[1]; result[2][0] = matrix[2]; result[3][0] = matrix[3];
    result[0][1] = matrix[4]; result[1][1] = matrix[5]; result[2][1] = matrix[6]; result[3][1] = matrix[7];
    result[0][2] = matrix[8]; result[1][2] = matrix[9]; result[2][2] = matrix[10]; result[3][2] = matrix[11];
    result[0][3] = matrix[12]; result[1][3] = matrix[13]; result[2][3] = matrix[14]; result[3][3] = matrix[15];

    return result;
	}

function extractNormalMatrix(matrix) { // This function computes the transpose of the inverse of 
    // the upperleft part (3X3) of the modelview matrix (see http://www.lighthouse3d.com/tutorials/glsl-tutorial/the-normal-matrix/ )

    var result = mat3();
    var upperleft = mat3();
    var tmp = mat3();

    upperleft[0][0] = matrix[0][0];  // if no scaling is performed, one can simply use the upper left
    upperleft[1][0] = matrix[1][0];  // part (3X3) of the modelview matrix
    upperleft[2][0] = matrix[2][0];

    upperleft[0][1] = matrix[0][1];
    upperleft[1][1] = matrix[1][1];
    upperleft[2][1] = matrix[2][1];

    upperleft[0][2] = matrix[0][2];
    upperleft[1][2] = matrix[1][2];
    upperleft[2][2] = matrix[2][2];

    tmp = matrixinvert(upperleft);
    result = transpose(tmp);

    return result;
}

function matrixinvert(matrix) {

    var result = mat3();

    var det = matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[2][1] * matrix[1][2]) -
                 matrix[0][1] * (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]) +
                 matrix[0][2] * (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);

    var invdet = 1 / det;

    // inverse of matrix m
    result[0][0] = (matrix[1][1] * matrix[2][2] - matrix[2][1] * matrix[1][2]) * invdet;
    result[0][1] = (matrix[0][2] * matrix[2][1] - matrix[0][1] * matrix[2][2]) * invdet;
    result[0][2] = (matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1]) * invdet;
    result[1][0] = (matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2]) * invdet;
    result[1][1] = (matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0]) * invdet;
    result[1][2] = (matrix[1][0] * matrix[0][2] - matrix[0][0] * matrix[1][2]) * invdet;
    result[2][0] = (matrix[1][0] * matrix[2][1] - matrix[2][0] * matrix[1][1]) * invdet;
    result[2][1] = (matrix[2][0] * matrix[0][1] - matrix[0][0] * matrix[2][1]) * invdet;
    result[2][2] = (matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]) * invdet;

    return result;
}


function createModel(modelData) {
    var model = {};
    model.coordsBuffer = gl.createBuffer();
    model.normalBuffer = gl.createBuffer();
    model.textureBuffer = gl.createBuffer();
    model.indexBuffer = gl.createBuffer();
    model.count = modelData.indices.length;

    gl.bindBuffer(gl.ARRAY_BUFFER, model.coordsBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, modelData.vertexPositions, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, model.normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, modelData.vertexNormals, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, model.textureBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, modelData.vertexTextureCoords, gl.STATIC_DRAW);

    console.log(modelData.vertexPositions.length);
    console.log(modelData.indices.length);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, modelData.indices, gl.STATIC_DRAW);

    model.render = function () {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.coordsBuffer);
        gl.vertexAttribPointer(CoordsLoc, 3, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
        gl.vertexAttribPointer(NormalLoc, 3, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureBuffer);
        gl.vertexAttribPointer(TexCoordLoc, 2, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);

        gl.uniformMatrix4fv(ModelviewLoc, false, flatten(modelview));    //--- load flattened modelview matrix
        gl.uniformMatrix3fv(NormalMatrixLoc, false, flatten(normalMatrix));  //--- load flattened normal matrix

        gl.drawElements(gl.TRIANGLES, this.count, gl.UNSIGNED_SHORT, 0);
        console.log(this.count);
    }
    return model;
}



function createProgram(gl, vertexShaderSource, fragmentShaderSource) {
    var vsh = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vsh, vertexShaderSource);
    gl.compileShader(vsh);
    if (!gl.getShaderParameter(vsh, gl.COMPILE_STATUS)) {
        throw "Error in vertex shader:  " + gl.getShaderInfoLog(vsh);
    }
    var fsh = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fsh, fragmentShaderSource);
    gl.compileShader(fsh);
    if (!gl.getShaderParameter(fsh, gl.COMPILE_STATUS)) {
        throw "Error in fragment shader:  " + gl.getShaderInfoLog(fsh);
    }
    var prog = gl.createProgram();
    gl.attachShader(prog, vsh);
    gl.attachShader(prog, fsh);
    gl.linkProgram(prog);
    if (!gl.getProgramParameter(prog, gl.LINK_STATUS)) {
        throw "Link error in program:  " + gl.getProgramInfoLog(prog);
    }
    return prog;
}


function getTextContent(elementID) {
    var element = document.getElementById(elementID);
    var fsource = "";
    var node = element.firstChild;
    var str = "";
    while (node) {
        if (node.nodeType == 3) // this is a text node
            str += node.textContent;
        node = node.nextSibling;
    }
    return str;
}


window.onload = function init() {
    try {
        var canvas = document.getElementById("glcanvas");
        gl = canvas.getContext("webgl");
        if (!gl) {
            gl = canvas.getContext("experimental-webgl");
        }
        if (!gl) {
            throw "Could not create WebGL context.";
        }

        // LOAD SHADER (standard texture mapping)
        var vertexShaderSource = getTextContent("vshader");
        var fragmentShaderSource = getTextContent("fshader");
        prog = createProgram(gl, vertexShaderSource, fragmentShaderSource);

        gl.useProgram(prog);

        // locate variables for further use
        CoordsLoc = gl.getAttribLocation(prog, "vcoords");
        NormalLoc = gl.getAttribLocation(prog, "vnormal");
        TexCoordLoc = gl.getAttribLocation(prog, "vtexcoord");

		// Initialize a texture
		initTexture();
		
        ModelviewLoc = gl.getUniformLocation(prog, "modelview");
        ProjectionLoc = gl.getUniformLocation(prog, "projection");
        NormalMatrixLoc = gl.getUniformLocation(prog, "normalMatrix");

        gl.enableVertexAttribArray(CoordsLoc);
        gl.enableVertexAttribArray(NormalLoc);
        gl.enableVertexAttribArray(TexCoordLoc);

        gl.enable(gl.DEPTH_TEST);

        //  create a "rotator" monitoring mouse mouvement
        rotator = new SimpleRotator(canvas, render);
        //  set initial camera position at z=40, with an "up" vector aligned with y axis
        //   (this defines the initial value of the modelview matrix )
        rotator.setView([0, 0, 1], [0, 1, 0], 40);

        // You can use basic models using the following lines

        sphere = createModel(uvSphere(10.0, 25.0, 25.0));
        cylinder = createModel(uvCylinder(10.0, 20.0, 25.0, false, false));
        box = createModel(cube(10.0));

		teapot = createModel(teapotModel);
        disk = createModel(ring(5.0, 10.0, 25.0));
        torus = createModel(uvTorus(15.0, 5.0, 25.0, 25.0));
        cone = createModel(uvCone(10.0, 20.0, 25.0, true));
		supportRadar = createModel(uvSupportRadar(10.0, 20.0, 25.0, true));
		radar= createModel(ring(2.5, 10.0, 25.0));
		cutCone=createModel(uvCutCone(10, 20.0, 32.0, false));
		moitieFourche=createModel(fourche(10.0));
		octogone=createModel(octo(10.0, 20.0, 8.0, false, false));
		semiSphere=createModel(uvSemiSphere(10.0, 25.0, 25.0));
 
        ambientProduct = mult(lightAmbient, materialAmbient);
        diffuseProduct = mult(lightDiffuse, materialDiffuse);
        specularProduct = mult(lightSpecular, materialSpecular);
    }
    catch (e) {
        document.getElementById("message").innerHTML =
             "Could not initialize WebGL: " + e;
        return;
    }

    setInterval(render, 1000);
}



